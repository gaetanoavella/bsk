package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void testGetFirstThrow() throws BowlingException{
		Frame frame = new Frame (2,4);
		assertEquals (2, frame.getFirstThrow());
	}
	
	@Test
	public void testGetSecondThrow() throws BowlingException{	
		Frame frame = new Frame (2,4);
		assertEquals (4, frame.getSecondThrow());
	}
	
	@Test
	public void testGetScoreFrame() throws BowlingException{	
		Frame frame = new Frame (2,6);
		assertEquals (8, frame.getScore());
	}	
}
