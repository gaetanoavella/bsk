package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	ArrayList <Frame> game;
	int score=0;
	private int firstBonusThrow;
	private int secondBonusThrow;
	
	public Game() {
		this.game = new ArrayList<>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		game.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return game.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {		
		if(isBestScore()) {
			score = 300;
			}else 
				for(int i=0;i<game.size();i++) {
					if(game.get(i).isStrike() && (i+1)<10) {
						if (game.get(i+1).getFirstThrow()==10) {
							score += game.get(i).getScore() + game.get(i+1).getScore() + game.get(i+2).getFirstThrow();
							}else score += game.get(i).getScore() + game.get(i+1).getScore();
					} else if (game.get(i).isSpare() && (i+1)<10) {
							score += game.get(i).getScore() + game.get(i+1).getFirstThrow();
							} else score += game.get(i).getScore();
					}
		lastFrame();
		return score;	
		}
	
	public boolean isBestScore() {
		for (int i=0;i<game.size();i++) {
			if (game.get(i).getFirstThrow() != 10) {
				return false;
				}
			}
		return true;
		}
	
	public void lastFrame () throws BowlingException {
		if(game.get(9).isStrike()) {
			setSecondBonusThrow(secondBonusThrow);
			score += getSecondBonusThrow();
			} else if (game.get(9).isSpare()) {
				setFirstBonusThrow(firstBonusThrow);
				score += getFirstBonusThrow();
				}
		}
	
}
